import { Component, OnInit } from '@angular/core';
import {NgbModal, ModalDismissReasons, NgbModalRef} from '@ng-bootstrap/ng-bootstrap';

@Component({
  selector: 'app-portfolio',
  templateUrl: './portfolio.component.html',
  styleUrls: ['./portfolio.component.scss']
})
export class PortfolioComponent implements OnInit {
  closeResult: string;
  modal: NgbModalRef

  constructor(private modalService: NgbModal) { }

  ngOnInit() {
  }

  open(content) {
    this.modal = this.modalService.open(content)
  }

}
