import { Component, OnInit, HostListener } from '@angular/core';
import { Router } from '@angular/router';

@Component({
  selector: 'app-navigation-menu',
  templateUrl: './navigation-menu.component.html',
  styleUrls: ['./navigation-menu.component.scss']
})
export class NavigationMenuComponent implements OnInit {
  public navigationPinned: boolean = false

  constructor(private router: Router) { }

  ngOnInit() {}

  @HostListener('window:scroll', ['$event'])
  onScroll(event) {
    let topOffset = window.pageYOffset || document.documentElement.scrollTop || document.body.scrollTop || 0;
    if (topOffset > 100) {
      this.navigationPinned = true;
    } else {
      this.navigationPinned = false;
    }
  }

  updateHash(event): void {
    if (!event) {
      this.router.navigate(['/'])
      return;
    }

    const locationId = event.target.attributes.href.value.slice(1)
    this.router.navigate( ['/'], {fragment: locationId});
  }

  isActive(clickedHash): boolean {
    return (window.location.hash === clickedHash.href.split('/')[3])
  }

}
