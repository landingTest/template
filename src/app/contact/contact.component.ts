import { Component, OnInit } from '@angular/core';
import { FormBuilder, Validators } from '@angular/forms';

@Component({
  selector: 'app-contact',
  templateUrl: './contact.component.html',
  styleUrls: ['./contact.component.scss']
})
export class ContactComponent implements OnInit {
  contactForm = this.fb.group({
    name: ['', Validators.required],
    email: ['', Validators.required],
    phone: ['', Validators.required],
    message: ['', Validators.required],
  })

  constructor(private fb: FormBuilder) { }

  ngOnInit() {}

  onSubmit() {}

  get name() { return this.contactForm.get('name')}
  get email() { return this.contactForm.get('email')}
  get phone() { return this.contactForm.get('phone')}
  get message() { return this.contactForm.get('message')}

}
